const express = require('express');
const path = require('path');
const ejs = require('ejs');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const BlogPost = require('./models/BlogPost.js');
const fileUpload = require('express-fileupload');


const app = new express();
mongoose.connect('mongodb://localhost/blogger_app_db', { useNewURLParser: true });


app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());




app.get('/', async (req, res) => {
	const blogposts = await BlogPost.find({});

	res.render('index', { blogposts });

});


app.get('/about', (req, res) => {
	res.render('about');

});


app.get('/contact', (req, res) => {
	res.render('contact');

});


app.get('/post/:id', async (req, res) => {
	const blogpost = await BlogPost.findById(req.params.id);

	res.render('post', { blogpost });

});


app.get('/posts/new', (req, res) => {
	res.render('create');

});


app.post('/posts/store', (req, res) => {
	let image = req.files.image;

	image.mv(path.resolve(__dirname, 'public/img', image.name), async (error) => {
		await BlogPost.create({ ...req.body, image:'/img/' + image.name });

		res.redirect('/');

	});

});


app.post('/posts/search', async (req, res) => {
	console.log(req.body.search);


	const blogposts = await BlogPost.find({});

	console.log(blogposts)


	res.render('index', { blogposts });

})


const customMiddleWare = (req, res, next) => {
	console.log('Custom middle ware called');
	next();
};
app.use(customMiddleWare);


// TODO add search functionality to index.js to search for posts.

app.listen(4000, () => {
	console.log('App listen on port 4000');

});